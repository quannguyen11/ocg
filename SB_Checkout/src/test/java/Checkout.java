import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Checkout extends Actions {
    private WebDriver driver;
    private String url;

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        url = "https://sb-test11q.myshopbase.net/";
    }

    @Test
    public void test(){
        driver.get(url);

        clickOnBtn(driver, "//div[@class='flex items-center justify-end col-lg-2']/a");
        inputValue(driver, "//input[@placeholder='Find all product and more...']", "Mama saurus 2");
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
